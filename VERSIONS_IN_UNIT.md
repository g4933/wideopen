## They seem to have GCC 5.2.0 for most of things. 
    But also seen references to GCC 4.8.4-2ubuntu1~14.04 and GCC 4.9.1

## Qt5.4.1

## --- libc6-dev ---

./lib/libdl.so.2 -> libdl-2.25.so
./lib/libc.so.6 -> libc-2.25.so
./lib/libm.so.6 -> libm-2.25.so
...



## --- weston 1.10.0 (gcc 5.2.0) ---
(Unsure here, IAS has base version 1.10.0, but weston binary also reports such version. However, initial commit for IAS started on weston 4.0.0. What is going on?)
They seem to get the version from <libweston/version.h>; but that doesn't necessarily mean its right, they could've had installed libweston 1.10.0 but ias has a diffrrent code, so the header would get the wrong version.
Meson.build is the one who ensures that the version is set, the problem tho, is that it seems to be set the right way... So maybe they don't build with meson but with autotools?

Real weston is AT LEAST 5.0.91.  and LESS than 6.0.92
IAS is AT LEAST 6.0.0 and LESS than 7.0.0
After some more checks, I feel confident that the weston version is 6.0.0 and that the IAS might be 6.0.4 

IAS shell has print_fps() from the initial commit...  No fucking clue! now I see references for the ias_backend that are too outdated. WTF?
 


## --- wayland ivi shell extensions (1.3.91 based on ldd for egl examples libilm libraries have this version) --- https://github.com/COVESA/wayland-ivi-extension/tree/1.3.91 ---
--- ivi-window-manager-api seems to be https://github.com/COVESA/wayland-ivi-extension/blob/1.3.91/ivi-layermanagement-api/ilmClient/src/ilm_client_wayland_platform.c

## --- wayland ---
/usr/lib/libwayland-cursor.so.0.0.0
./usr/lib/libwayland-egl.so.1.0.0
./usr/lib/libwayland-client.so.0.3.0
./usr/lib/libwayland-server.so.0.1.0



## --- useful to get info ---
for file in $(find ./ -type f -exec bash -c 'file {} ' \; | awk '{print $1}' | sed "s/://"); do strings -a $file | grep -i "1.10" | sed "s#^#$file => #" ; done;

