---
title: Update is stuck at 0% or update files not read
---

:::danger
If you are in this state **DO NOT** attempt to remove `navi_extended` or `systemd` service installed. Doing so will remove our ability to execute commands, and thus will impede fixing the unit.  **FIRST ENSURE UPDATES WORK** and *then* worry about the removal of the aformentioned stuff.
:::

In some cases, some units (Russian so far) are extremely unhappy with **our** `DecryptToPIPE` script. We have a couple of variations of this script for different purposes, but regardless of the script used, the recovery process should be the same.  

## Step 1: Ensuring you have a failsafe to regain access.
Whether you decide to leave `navi_extended` or decide to install our `systemd` service, you **MUST** ensure those are working exactly as you expect it. 

## Step 2: Restore DecryptToPIPE_OG from the backups. 
In theory, you should have a copy of your `DecryptToPIPE_OG` on the headunit, and one should've also been made to your USB at the time of hacking it. 

That being said, use [**this script**](https://gitlab.com/g4933/gen5w/navi_extended/-/blob/main/USB_FILES/EXTREMELY_RISKY_BECAREFUL/restore_decrypttopipe_og.sh) as the code in `main_loop_code.sh` if you use `navi_extended` or as your code inside `wideopen_service.sh` if you are using the `systemd` service. 

Your unit should restart afterwards and your updates should work as usual.