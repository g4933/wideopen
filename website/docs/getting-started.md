---
title: Getting Started
slug: /
---

## What is this all about?

This project aims to give users an ownership of the headunit in their cars. You have paid for your car, you own it. This includes its computers. With this project we aim to provide you with an ability to extend your stock functionality on certain Korean models. 

We are not doing this for a commercial gain, however, you can use whatever we have learnt so far to build and sell Custom Firmware (CFW) if you wish, that is up to you. We only ask you to contribute back and to share your findings. Basically, an open source mentality is what we care about. At the end of the day, we all want the same: to have a complete control over our cars and their systems. 

* We do not sell the access to the hack. 
* We do not sell the tools to get access to the system. 
* We share our knowledge. 

You can: 
* Build and sell your CFW using the tools / knowledge that comes from the collaboration (please, strongly consider sharing how things are done so we can attract more developers when we decide to go "public").

Also, please consider safety first. Let's try not to allow non-technical people to drive while watching Netflix. Of course, this is completely at your discretion.
