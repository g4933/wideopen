---
title: What can be done so far?
---

:::caution
Anything done is at your own risk. Breaking this unit **IS POSSIBLE**; so be extremely careful. Read twice. Ask before doing something or if something is not crystal clear to you. 
:::

## Run arbitrary shell codes in a loop from our USB
We've created `navi_extended` which is used to replace official `AppNavi` (which leads to loosing navigation on the headunit!), by doing so, it enables us to run arbitrary codes on the headunit, this is the very first step to persist you access on the unit. Official `AppNavi` can be restored later.

[Read More...](https://gitlab.com/g4933/gen5w/navi_extended/-/blob/main/README.md)

## Extract decryption keys from the headunit
Thanks to the ability to run arbitrary shell codes, we can leverage that to extract the headunit's decryption keys and binaries, so we can perform decryption over updates. 

[Read More...](https://gitlab.com/g4933/gen5w/navi_extended/-/blob/main/README.md)


## Decrypt official update packages via Docker 
With the key extracted and the binary `DecryptToPIPE`, we can now build our very own Docker image to decrypt any update.

[Read More...](https://gitlab.com/g4933/gen5w/update_decryptor/-/blob/main/README.md)

## Enable WIFI + SSH for interactive shell
Another benefit of running arbitrary code via the loop on your USB, is that we can now enable wifi and SSH so we can create an interactive shell.

[Read More...](https://gitlab.com/g4933/gen5w/main-loops/wifi-ssh/-/blob/main/README.md)

## Disable encryption for updates (partial or total)
The process of disabling the encryption its quite easy. By leveraging our ability to run arbitrary codes via the main loop from our USB we can create a file (`/update/disableEncryptionUpdate`) with no content, this will disable the encryption.

[Read more here](https://gitlab.com/g4933/gen5w/update-patcher/-/tree/main)

## Install a service on systemd for a resilient loop
This is by far the safest and easiest way to keep executing stuff from your unit with the least impact to bricking the unit. Creating our own systemd service means that if we break our script, it only affects our script, instead of breaking any functional part of the system. All of this can be done thanks to the ability to run arbitrary codes via the main loop from our USB.

* [Service file defitnion](https://gitlab.com/g4933/gen5w/navi_extended/-/blob/main/USB_FILES/wideopen.service)
* [Entrypoint in USB for our service](https://gitlab.com/g4933/gen5w/navi_extended/-/blob/main/USB_FILES/wideopen_service.sh)
* [Code to install the service](https://gitlab.com/g4933/gen5w/navi_extended/-/blob/main/USB_FILES/install_wideopen_service.sh)

