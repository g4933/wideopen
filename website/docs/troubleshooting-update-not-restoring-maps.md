---
title: Update is not reintalling navigation
---

:::danger
If you are in this state **DO NOT** attempt to remove `navi_extended` or `systemd` service installed. Doing so will remove our ability to execute commands, and thus will impede fixing the unit. **FIRST ENSURE UPDATES WORK** and *then* worry about the removal of the aformentioned stuff.
:::

In some cases, some units (Russian so far) the update doesn't seem to reinstall the original navigation. We have yet to figure out th root cause of this issue, the it might be that the unit is ignoring the update because it can't find the current version (since `navi_extended` doesnt reply to the version pings) or maybe he is detecting something not right on the `AppNavi.tar` or on the `naviX.tar` files. 

:::info
It has been observed that if there are invalid files on the same directory as `AppNavi.tar` with the word `navi` on them, it might lead to the unit failing to update the navigation. Furthermore, if there's more entries on the md5 file than `naviX.tar` files on the directory or mistmatching md5 hashes, the update might fail or not proceed with the navigation update.
:::

## Step 1: Ensuring you have a failsafe to regain access.
Before doing anything, you **MUST** install our `systemd` - `wideopen` service and you **MUST** ensure that its working exactly as you expect it. 
Otherwise, if the replacement of AppNavi fails, you will **most likely** leave `navi_extended` corrupted and loose any access to execute arbitrary code to try to fix the issue. 

## Step 2: Restore original AppNavi
:::danger
This hasn't been tested enough! This is *unstable* and prone to race conditions.
:::

### Find the original AppNavi
You must find an original AppNavi, if it matches your OS version, it's much better. You can use the update decryptor to get the `AppNavi` from `AppNavi.tar/Bin`.

### Preparing the USB 
Please note that all the process is case-sensitive. 
* On your USB create a folder named `Bin` on the root. 
* Place on `Bin` the original `AppNavi` (`<USB_ROOT>/Bin/AppNavi`)

So your USB folder structure should look like 
```bash
<USB_ROOT>
├── Bin                 # Case sensitive!
│   └── AppNavi         # Original AppNavi
├── main_loop.sh        # Optional, depends on how you've setup your stuff
├── main_loop_code.sh   # Optional, depends on how you've setup your stuff
└── wideopen_service.sh # contents of https://gitlab.com/g4933/gen5w/navi_extended/-/blob/main/USB_FILES/EXTREMELY_RISKY_BECAREFUL/update_navi_manually.sh
```

### Running the scripts
Whether you are using navi_extended to do its own final stroke, or you are using our `systemd` - `wideopen` service, you should just put the contents of [this script](https://gitlab.com/g4933/gen5w/navi_extended/-/blob/main/USB_FILES/EXTREMELY_RISKY_BECAREFUL/update_navi_manually.sh) into the right place (see folder structure above). Plug your USB into your car and it should do the restore and reboot. If all goes well you should have your maps back.  