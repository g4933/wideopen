---
title: Where's the stuff?
---

## Useful Links
* Our codes are here: https://gitlab.com/g4933
* A wiki will be filled over time at https://gitlab.com/g4933/wideopen/-/wikis/


## Ways of contact
* Telegram: https://t.me/+GeTwB_AD7utjNmVk
* Email: gen5wideopen@protonmail.com
* Youtube: https://www.youtube.com/channel/UCtr0qvjyIvX6n-dipUHchwg
