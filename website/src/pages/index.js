import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'Who is this for?',
    imageUrl: 'img/programmer.svg',
    description: (
      <>
        Currently this is targetting only experienced developers, this can
        and will go wrong!, therefore at least a decent level of experince
        with linux is required, but anybody is welcome. We are open and it
        will always remain open!.
      </>
    ),
  },
  {
    title: 'Our goal',
    imageUrl: 'img/we.svg',
    description: (
      <>
        Allowing developers to be able to create new experiences on the headunits
        of our cars in a safe and reliable way. We'd love to see this coming straight
        from the manufacturers, providing a safe sandboxed environment for devs to 
        play and tinker with their device without compromising the vehicle security
        or functionality. 
      </>
    ),
  },
  {
    title: 'Why we created this?',
    imageUrl: 'img/why.svg',
    description: (
      <>
        Tired of being limited on what we can do with the devices we have
        bought. You own your car and are free to make any modification you
        see fit, as long as it doesn't compromise security, why can't we 
        own the headunits on our cars?.
        {/* ahead and move your docs into the <code>docs</code> directory. */}
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

export default function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="HKMC Gen5W headunit modification.">
      <header className={clsx('hero hero--dark', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--primary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              View our Docs!
            </Link>
            <Link
              className={clsx(
                'button button--info button--lg',
                styles.getStarted,
              )}
              to="https://gitlab.com/g4933/wideopen/-/wikis/home">
              Visit our Wiki!
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}
